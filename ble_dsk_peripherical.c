/* =========================================================================
 * This module is initializing a ble_peripherical device from a simple
 * given structure and create all the needed services & charcarteristics
 * -------------------------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * You can obtain a non GPL version of this program by contacting me
 * on www.disk91.com
 * --------------------------------------------------------------------------
 * (c) 2015 Paul Pinault / disk91.com
 * --------------------------------------------------------------------------
 * 2015-12-19 - creation
 * ==========================================================================
 * usage :
 * ==========================================================================
 */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "app_timer.h"
#include "ble_hci.h"
#include "ble_dsk_peripherical.h"
#include "ble_dsk_service.h"
#include "softdevice_handler.h"
#include "nrf_delay.h"

#define CENTRAL_LINK_COUNT               0                                          /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT            1  



/* ---------------------------------------------------------------------
 * Global variable with all elements on peripherical
 * ---------------------------------------------------------------------
 */
ble_dsk_dyn_peripherical_t	ble_dsk_dyn_peripherical;
static ble_dsk_dyn_peripherical_t * _p = &ble_dsk_dyn_peripherical;

/* ---------------------------------------------------------------------
 * Init Ble Peripherical
 * ---------------------------------------------------------------------
 */
void ble_dsk_peripherical_init(const ble_dsk_peripherical_t * _init)
{
    uint32_t err_code;

	// --------------------------------------------
	// create and initialize the ble_dsk_dyn_periph structure
	_p->peripherical = _init;
	
	// --------------------------------------------
	// init system elements
	err_code = pstorage_init();
	APP_ERROR_CHECK(err_code);

	
	
	// --------------------------------------------
	// init softdevice (req - softdevice_handler.h) 
	uint8_t enabled;
	sd_softdevice_is_enabled(&enabled);
	if(!enabled) {
#if defined(__GNUC__)
		nrf_clock_lf_cfg_t clock_lf_cfg =  {
				.source        = NRF_CLOCK_LF_SRC_XTAL,
				.rc_ctiv       = 0,
				.rc_temp_ctiv  = 0,
				.xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM
		};

		// Initialize the SoftDevice handler module.
		SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
#else
		SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, NULL);
#endif
	}

#if defined(S110) || defined(S130) || defined(S132)
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));
#if (defined(S130) || defined(S132))
    ble_enable_params.gatts_enable_params.attr_tab_size   = BLE_GATTS_ATTR_TAB_SIZE_DEFAULT;
#endif

	err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
													PERIPHERAL_LINK_COUNT,
													&ble_enable_params);
	APP_ERROR_CHECK(err_code);
  

#if WITH_DFU_SERVCICE == 1	
	STATIC_ASSERT(BLE_DSK_CONFIG_SERVICE_CHANGE_CHAR);                                     /** When having DFU Service support in application the Service Changed Characteristic should always be present. */
#endif
    ble_enable_params.gatts_enable_params.service_changed = _p->peripherical->config_bitfield & BLE_DSK_CONFIG_SERVICE_CHANGE_CHAR;
    // Enable BLE stack.
		
	if(!enabled) {	
		err_code = softdevice_enable(&ble_enable_params);
		APP_ERROR_CHECK(err_code);
	}
#endif

	  //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
		
		// ----------------------------------------------
		// Device manager init
		{
			dm_init_param_t        init_param = {.clear_persistent_data = _p->erase_bonds};
			dm_application_param_t register_param;
						
			err_code = dm_init(&init_param);
			APP_ERROR_CHECK(err_code);

			memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));
			register_param.sec_param.bond         = ((_p->peripherical->sec_options & BLE_DSK_SEC_OPTIONS_BONDING) != 0 )?1:0;
			register_param.sec_param.mitm         = ((_p->peripherical->sec_options & BLE_DSK_SEC_OPTIONS_MITM) != 0 )?0:1;
			register_param.sec_param.io_caps      = _p->peripherical->sec_io_capabilities;
			register_param.sec_param.oob          = _p->peripherical->sec_oob_data;
			register_param.sec_param.min_key_size = _p->peripherical->sec_min_key_size;
			register_param.sec_param.max_key_size = _p->peripherical->sec_max_key_size;
			register_param.evt_handler            = device_manager_evt_handler;
			register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

			err_code = dm_register(&_p->dm_app_handle, &register_param);
			APP_ERROR_CHECK(err_code);
		}
		
		// ----------------------------------------------
		// GAP (Generic Access Profile) init
		{
			ble_gap_conn_params_t   gap_conn_params;
			ble_gap_conn_sec_mode_t sec_mode;
			if ( (_p->peripherical->sec_options == BLE_DSK_SEC_OPTIONS_NONE) ) {
				BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
			} else if ( (_p->peripherical->sec_options & BLE_DSK_SEC_OPTIONS_ENCRYPTION) != 0 ) {
				if ( (_p->peripherical->sec_options & BLE_DSK_SEC_OPTIONS_MITM) != 0 ) {
					BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&sec_mode);
				} else {
					BLE_GAP_CONN_SEC_MODE_SET_ENC_NO_MITM(&sec_mode);
				}
			} else if ( (_p->peripherical->sec_options & BLE_DSK_SEC_OPTIONS_SIGNING) != 0 ) {
				if ( (_p->peripherical->sec_options & BLE_DSK_SEC_OPTIONS_MITM) != 0 ) {
					BLE_GAP_CONN_SEC_MODE_SET_SIGNED_WITH_MITM(&sec_mode);
				} else {
					BLE_GAP_CONN_SEC_MODE_SET_SIGNED_NO_MITM(&sec_mode);
				}
			}
			if ( _p->peripherical->add_mac_address ) {
				// @TODO - implement mac address addition at end of the device name...
			} else {
			   err_code = sd_ble_gap_device_name_set(&sec_mode,(const uint8_t *)(_p->peripherical->periph_name),strlen(_p->peripherical->periph_name));
			}
			APP_ERROR_CHECK(err_code);

			err_code = sd_ble_gap_tx_power_set(_p->peripherical->tx_power);
			APP_ERROR_CHECK(err_code);

			err_code = sd_ble_gap_appearance_set(_p->peripherical->appearance_type);
			APP_ERROR_CHECK(err_code);

			memset(&gap_conn_params, 0, sizeof(gap_conn_params));
			gap_conn_params.min_conn_interval = _p->peripherical->gap_min_cnx_interval;
			gap_conn_params.max_conn_interval = _p->peripherical->gap_max_cnx_interval;
			gap_conn_params.slave_latency     = _p->peripherical->gap_slave_latency;
			gap_conn_params.conn_sup_timeout  = _p->peripherical->gap_cnx_sup_timeout;
			err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
			APP_ERROR_CHECK(err_code);
		}
		// --------------------------------------------------
		// Advertizing init
		{
			uint8_t i;
			memset(&_p->adv_data, 0, sizeof(_p->adv_data));
			_p->adv_data.name_type               = BLE_ADVDATA_FULL_NAME;														// Display full name				
			_p->adv_data.include_appearance      = true;																						// Visible ?
			_p->adv_data.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;			// GENERAL mode = alway active compared to LIMITED = 30s
																																															// with classic bluetooth = Basic Rate (1Mbps) Enhanced Rate (2-3Mbps) not supported
			
			// create service / uuid list
			_p->service_count = 0;
#if WITH_BAS_SERVICE == 1
			if ( (_p->peripherical->basic_service & BLE_DSK_SERVICE_BAS) != 0 ) {
					_p->m_adv_uuids[_p->service_count].uuid = BLE_UUID_BATTERY_SERVICE;
				  _p->m_adv_uuids[_p->service_count].type = BLE_UUID_TYPE_BLE;
				  _p->service_count++;
			}
#endif
#if WITH_DIS_SERVICE == 1
			if ( (_p->peripherical->basic_service & BLE_DSK_SERVICE_DIS) != 0 ) {
					_p->m_adv_uuids[_p->service_count].uuid = BLE_UUID_DEVICE_INFORMATION_SERVICE;
				  _p->m_adv_uuids[_p->service_count].type = BLE_UUID_TYPE_BLE;
				  _p->service_count++;
			}
#endif
#if WITH_DFU_SERVICE == 1
		/*	if ( (_p->peripherical->basic_service & BLE_DSK_SERVICE_DFU) != 0 ) {
				_p->m_adv_uuids[_p->service_count].uuid = BLE_UUID_BATTERY_SERVICE;
				_p->m_adv_uuids[_p->service_count].type = BLE_UUID_TYPE_BLE;
				// @TODO - DFU Initiate ...
				_p->service_count++;
			}*/
#endif
			_p->uuid_type = BLE_UUID_TYPE_BLE;
			if ( _p->peripherical->uuid_128 ) {
				err_code = sd_ble_uuid_vs_add(&_p->peripherical->nus_base_uuid, &_p->uuid_type);
				APP_ERROR_CHECK(err_code);
			}
			for ( i = 0 ; i < _p->peripherical->service_count ; i++ ) {
				_p->m_adv_uuids[_p->service_count].uuid = _p->peripherical->services[i].service_uuid;
				_p->m_adv_uuids[_p->service_count].type = _p->uuid_type;
				_p->service_handler[i] = ble_dsk_service_allocated(&_p->peripherical->services[i],&_p->m_adv_uuids[_p->service_count]);	
				if ( _p->service_handler[i] == __BLE_DSK_ERROR_ERROR ){
					APP_ERROR_CHECK(NRF_ERROR_FORBIDDEN);
				}
				_p->service_count++;
			}
						
			_p->adv_data.uuids_complete.uuid_cnt = _p->service_count;
			_p->adv_data.uuids_complete.p_uuids  = _p->m_adv_uuids;

			advertising_init(_p->peripherical->advertising_mode, _p->peripherical->advertising_dir_fast_interval, _p->peripherical->advertising_dir_fast_timeout,
																													 _p->peripherical->advertising_slow_interval, _p->peripherical->advertising_slow_timeout);
	  }
		
		// ----------------------------------------------------------------------------------
		// Service init
#if WITH_BAS_SERVICE == 1
		{
			// Battery service
			if ( ( _p->peripherical->basic_service & BLE_DSK_SERVICE_BAS ) != 0 ) {
				ble_bas_init_t  bas_init;
			  memset(&bas_init, 0, sizeof(bas_init));

			  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.cccd_write_perm);
				BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.read_perm);
				BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&bas_init.battery_level_char_attr_md.write_perm);
				BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_report_read_perm);
			  bas_init.evt_handler          = NULL;
				bas_init.support_notification = true;
				bas_init.p_report_ref         = NULL;
				if ( _p->peripherical->getBatteryInfo != NULL ) {
					bas_init.initial_batt_level = _p->peripherical->getBatteryInfo();
				} else {
					bas_init.initial_batt_level = 0;
				}
				err_code = ble_bas_init(&_p->m_bas, &bas_init);
			  APP_ERROR_CHECK(err_code);
			}
		}
#endif
#if WITH_DIS_SERVICE == 1
		{
			// Device information service
			if ( ( _p->peripherical->basic_service & BLE_DSK_SERVICE_DIS ) != 0 ) {
				ble_dis_init_t	dis_init;
				char strTmp[__BLE_DSK_MAX_STRING_SIZE];
				memcpy(&strTmp,_p->peripherical->manuf_name,__BLE_DSK_MAX_STRING_SIZE);
				memset(&dis_init, 0, sizeof(dis_init));
			  ble_srv_ascii_to_utf8(&dis_init.manufact_name_str,strTmp);
			  BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
				BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);
				err_code = ble_dis_init(&dis_init);
				APP_ERROR_CHECK(err_code);
			}
		}
#endif		
#if WITH_DFU_SERVICE == 1
		{
			// Device Firmware Update service
			if ( ( _p->peripherical->basic_service & BLE_DSK_SERVICE_DFU ) != 0 ) {
					/** @snippet [DFU BLE Service initialization] */
					ble_dfu_init_t   dfus_init;

					// Initialize the Device Firmware Update Service.
					memset(&dfus_init, 0, sizeof(dfus_init));

					dfus_init.evt_handler   = dfu_app_on_dfu_evt;
					dfus_init.error_handler = NULL;
					dfus_init.evt_handler   = dfu_app_on_dfu_evt;
					dfus_init.revision      = DFU_REVISION;

					err_code = ble_dfu_init(&_p->m_dfus, &dfus_init);
					APP_ERROR_CHECK(err_code);

					dfu_app_reset_prepare_set(reset_prepare);
					dfu_app_dm_appl_instance_set(_p->m_app_handle);
					/** @snippet [DFU BLE Service initialization] */
			}
		}
#endif		
		
		// Custom Services creation - init all at once
	  ble_dsk_service_init();	
		
		// ----------------------------------------------------------------------------------
		// Connection init - manage timing once the device is connected to central
	  {
			ble_conn_params_init_t cp_init;

			_p->m_conn_handle = BLE_CONN_HANDLE_INVALID;						// will be set on a connection event
			
			memset(&cp_init, 0, sizeof(cp_init));
			cp_init.p_conn_params                  = NULL;
			cp_init.first_conn_params_update_delay = APP_TIMER_TICKS((_p->peripherical->cnx_time_from_connect_to_update*100), APP_TIMER_PRESCALER);
			cp_init.next_conn_params_update_delay  = APP_TIMER_TICKS((_p->peripherical->cnx_time_between_update*100), APP_TIMER_PRESCALER);;
			cp_init.max_conn_params_update_count   = _p->peripherical->cnx_max_cnx_attempt;
			cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
			cp_init.disconnect_on_fail             = _p->peripherical->cnx_disconnect_on_fail;
			cp_init.evt_handler                    = on_conn_params_evt;
			cp_init.error_handler                  = conn_params_error_handler;

			err_code = ble_conn_params_init(&cp_init);
			APP_ERROR_CHECK(err_code);		
		}
	
		// -------------------------------------------------------------------------------------
		// Go for it
		if ( (_p->peripherical->advertising_mode & BLE_DSK_ADV_MODE_DIRECT) != 0 ) {
			err_code = ble_advertising_start(BLE_ADV_MODE_DIRECTED);
		} else if ( (_p->peripherical->advertising_mode & BLE_DSK_ADV_MODE_FAST) != 0 ) {
			err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
		} else if ( (_p->peripherical->advertising_mode & BLE_DSK_ADV_MODE_SLOW) != 0 ) {
			err_code = ble_advertising_start(BLE_ADV_MODE_SLOW);
		}
		
    APP_ERROR_CHECK(err_code);
}

/* ---------------------------------------------------------------------------
 * start advertizing - this procedure can be call each time advertizing is
 *                     required as it stops after timeout
 * input :
 *     mode : list of advertizing mode your want to support (see BLE_DSK_ADV_MODE_XXX)
 *            cumulative. The excution order is Directed >> Fast >> Slow >> Idle
 *     dir_fast_interval : is the advertizing interval for Fast mode in MS
 *     dir_fast_timeout  : is the timeout Fast mode in S
 *     slow_interval     : is the advertizing interval for SLOW and directed slow mode in MS
* 		 slow_timeout			 : is the timeout for SLOW and Directed SLOW mode in 1 and retry time
 * ---------------------------------------------------------------------------
 */
void advertising_init(uint8_t mode, uint16_t dir_fast_interval, uint32_t dir_fast_timeout, uint16_t slow_interval, uint32_t slow_timeout)
{
      uint32_t      					err_code;
			ble_adv_modes_config_t	adv_config;
	
			memset(&adv_config, 0, sizeof(adv_config));
			if ( (mode & BLE_DSK_ADV_MODE_WHITELIST) != 0 ) {
				adv_config.ble_adv_whitelist_enabled = BLE_ADV_WHITELIST_ENABLED;
			} else {
				adv_config.ble_adv_whitelist_enabled = BLE_ADV_WHITELIST_DISABLED;
			}
			if ( (mode & BLE_DSK_ADV_MODE_DIRECT) != 0 ) {
				adv_config.ble_adv_directed_enabled = BLE_ADV_DIRECTED_ENABLED;
			} else {
				adv_config.ble_adv_directed_enabled = BLE_ADV_DIRECTED_DISABLED;
			}
			if ( (mode & BLE_DSK_ADV_MODE_DIRECT_SLOW) != 0 ) {
				adv_config.ble_adv_directed_slow_enabled  = BLE_ADV_DIRECTED_SLOW_ENABLED;
				adv_config.ble_adv_directed_slow_interval = (8*slow_interval)/5; 					// convert to 0.625 steps
				adv_config.ble_adv_directed_slow_timeout  = slow_timeout;
			} else {
				adv_config.ble_adv_directed_slow_enabled = BLE_ADV_DIRECTED_SLOW_DISABLED;
			}
			if ( (mode & BLE_DSK_ADV_MODE_SLOW) != 0 ) {
				adv_config.ble_adv_slow_enabled  = BLE_ADV_SLOW_ENABLED;
				adv_config.ble_adv_slow_interval = (8*slow_interval)/5; 									// convert to 0.625 steps
				adv_config.ble_adv_slow_timeout  = slow_timeout;
			} else {
				adv_config.ble_adv_slow_enabled = BLE_ADV_SLOW_DISABLED;
			}
			if ( (mode & BLE_DSK_ADV_MODE_FAST) != 0 ) {
				adv_config.ble_adv_fast_enabled  = BLE_ADV_FAST_ENABLED;
				adv_config.ble_adv_fast_interval = (8*dir_fast_interval)/5; 							// convert to 0.625 steps
				adv_config.ble_adv_fast_timeout  = dir_fast_timeout;
			} else {
				adv_config.ble_adv_fast_enabled = BLE_ADV_FAST_DISABLED;
			}
	    err_code = ble_advertising_init(&_p->adv_data, NULL, &adv_config, on_adv_evt, NULL);
      APP_ERROR_CHECK(err_code);
}


/* ---------------------------------------------------------------------------
 * Manage ble event and dispatch to the right services / characterisic
 * This function is call on any ble event (read/write/notify/connect...) 
 * ---------------------------------------------------------------------------
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
	  // Manage BLE events
    switch (p_ble_evt->header.evt_id)
    {
      case BLE_GAP_EVT_CONNECTED:
         _p->m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
         break;

      case BLE_GAP_EVT_DISCONNECTED:
         _p->m_conn_handle = BLE_CONN_HANDLE_INVALID;
         break;
      default:
         break;
		}			
	
		// default system event handler
    dm_ble_evt_handler(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_advertising_on_ble_evt(p_ble_evt);
	
		// service event handler
#if WITH_BAS_SERVICE == 1
	  if ( (_p->peripherical->basic_service & BLE_DSK_SERVICE_BAS) != 0 )
       ble_bas_on_ble_evt(&_p->m_bas, p_ble_evt);
#endif
		
#if WITH_DFU_SERVICE == 1
    /** @snippet [Propagating BLE Stack events to DFU Service] */
    ble_dfu_on_ble_evt(&_p->m_dfus, p_ble_evt);
    /** @snippet [Propagating BLE Stack events to DFU Service] */
#endif
		// custom services
    ble_dsk_on_event_dispatch(p_ble_evt); 
		
}



/* --------------------------------------------------------------------------
 * Manage advertising events - send when advertsing starts
 * --------------------------------------------------------------------------
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
			case BLE_ADV_EVT_SLOW:
			case BLE_ADV_EVT_DIRECTED:
			case BLE_ADV_EVT_DIRECTED_SLOW:
      case BLE_ADV_EVT_FAST:
					if( _p->peripherical->on_start_advertizing != NULL) 
						_p->peripherical->on_start_advertizing(ble_adv_evt);
					break;
      case BLE_ADV_EVT_IDLE:
					if ( _p->peripherical->on_stop_advertizing != NULL)
						_p->peripherical->on_stop_advertizing(ble_adv_evt);
					break;
      default:
          break;
    }
}


/* --------------------------------------------------------------------------
 * Manage connection parameter events
 *   call on disconnection when disconnect_on_fail is set to true
 * --------------------------------------------------------------------------
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(_p->m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}

static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/* --------------------------------------------------------------------------
 * Manage system events
 * --------------------------------------------------------------------------
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    pstorage_sys_event_handler(sys_evt);				// needed at list for long term bonding
    ble_advertising_on_sys_evt(sys_evt);
}

/* --------------------------------------------------------------------------
 * Manage Device Manager events
 * --------------------------------------------------------------------------
 */
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t  const * p_event,
                                           ret_code_t          event_result)
{
    APP_ERROR_CHECK(event_result);

#if WITH_DFU_SERVICE == 1
    if (   (_p->peripherical->basic_service &  BLE_DSK_SERVICE_DFU ) != 0
			   && p_event->event_id == DM_EVT_LINK_SECURED) {
        app_context_load(p_handle);
    }
#endif 
		
    return NRF_SUCCESS;
}

#if WITH_DFU_SERVICE == 1
/**@brief Function for stopping advertising.
 */
static void advertising_stop(void)
{
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);

   // err_code = bsp_indication_set(BSP_INDICATE_IDLE);
   // APP_ERROR_CHECK(err_code);
}


/**@brief Function for loading application-specific context after establishing a secure connection.
 *
 * @details This function will load the application context and check if the ATT table is marked as
 *          changed. If the ATT table is marked as changed, a Service Changed Indication
 *          is sent to the peer if the Service Changed CCCD is set to indicate.
 *
 * @param[in] p_handle The Device Manager handle that identifies the connection for which the context
 *                     should be loaded.
 */
static void app_context_load(dm_handle_t const * p_handle)
{
    uint32_t                 err_code;
    static uint32_t          context_data;
    dm_application_context_t context;

    context.len    = sizeof(context_data);
    context.p_data = (uint8_t *)&context_data;

    err_code = dm_application_context_get(p_handle, &context);
    if (err_code == NRF_SUCCESS)
    {
        // Send Service Changed Indication if ATT table has changed.
        if ((context_data & (DFU_APP_ATT_TABLE_CHANGED << DFU_APP_ATT_TABLE_POS)) != 0)
        {
            err_code = sd_ble_gatts_service_changed(_p->m_conn_handle, APP_SERVICE_HANDLE_START, BLE_HANDLE_MAX);
            if ((err_code != NRF_SUCCESS) &&
                (err_code != BLE_ERROR_INVALID_CONN_HANDLE) &&
                (err_code != NRF_ERROR_INVALID_STATE) &&
                (err_code != BLE_ERROR_NO_TX_PACKETS) &&
                (err_code != NRF_ERROR_BUSY) &&
                (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING))
            {
                APP_ERROR_HANDLER(err_code);
            }
        }

        err_code = dm_application_context_delete(p_handle);
        APP_ERROR_CHECK(err_code);
    }
    else if (err_code == DM_NO_APP_CONTEXT)
    {
        // No context available. Ignore.
    }
    else
    {
        APP_ERROR_HANDLER(err_code);
    }
}


/** @snippet [DFU BLE Reset prepare] */
/**@brief Function for preparing for system reset.
 *
 * @details This function implements @ref dfu_app_reset_prepare_t. It will be called by
 *          @ref dfu_app_handler.c before entering the bootloader/DFU.
 *          This allows the current running application to shut down gracefully.
 */
static void reset_prepare(void)
{
    uint32_t err_code;

    if (_p->m_conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        // Disconnect from peer.
        err_code = sd_ble_gap_disconnect(_p->m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
        //err_code = bsp_indication_set(BSP_INDICATE_IDLE);
        //APP_ERROR_CHECK(err_code);
    }
    else
    {
        // If not connected, the device will be advertising. Hence stop the advertising.
        advertising_stop();
    }

    err_code = ble_conn_params_stop();
    APP_ERROR_CHECK(err_code);

    nrf_delay_ms(500);
}
/** @snippet [DFU BLE Reset prepare] */
#endif 
